import axios from '@/libs/api.request'

export default {
    login(data){
        return axios.request({
            url:'login',
            data,
            method:'post'
        })
    }
}