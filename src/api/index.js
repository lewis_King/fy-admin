let Api={};
let requireContext = require.context('./', false, /\.js$/)
requireContext.keys().forEach(path => {
    let apiObject = requireContext(path).default
    let key = path.match(/\.\/(\w+)/)[1]
    Api[key] = apiObject
});
export default Api