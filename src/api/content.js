import axios from '@/libs/api.request'

export default {
    //获取文章管理列表
    articleList(id){
        return axios.request({
            url:'bbs/list/'+id,
            method:'get'
        })
    }
}