import axios from '@/libs/api.request'

export default {
    //list
    //获取咨询管理列表
    consultManage(id){
        return axios.request({
            url:'bbs/list/'+id,
            method:'get'
        })
    },
    //获取律师擅长领域
    fieldManage(id){
        return axios.request({
            url:'setting/position/'+id,
            method:'get'
        })
    },
    //获取全部法律服务列表
    serviceManage(id){
        return axios.request({
            url:'service/list/'+id,
            method:'get'
        })
    },
    //delete
    //删除领域
    fieldDel(id){
        return axios.request({
            url:'setting/position/'+id,
            method:'DELETE'
        })
    },
    //删除法律服务
    serviceDel(id){
        return axios.request({
            url:'setting/service/'+id,
            method:'DELETE'
        })
    },
    //删除咨询
    questionDel(id){
        return axios.request({
            url:'setting/question/'+id,
            method:'DELETE'
        })
    },
    //add
    //增加法律服务类型
    serviceAdd(data){
        return axios.request({
            url:'setting/service/',
            method:'post',
            data
        })
    }
}