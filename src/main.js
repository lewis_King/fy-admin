import Vue from 'vue';
import iView from 'iview';
import { router } from './router/index';
import store from './store';
import App from './app.vue';
import util from '@/libs/util.js';
import hasPermission from '@/libs/hasPermission.js';
import 'iview/dist/styles/iview.css';
import {Upload,Dialog} from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css';
import Cookies from 'js-cookie';

Vue.use(iView);
Vue.use(hasPermission);
Vue.use(Upload)
Vue.use(Dialog)
Vue.prototype.$cookie=Cookies;
new Vue({
    el: '#app',
    router: router,
    store: store,
    render: h => h(App),
    mounted () {
        // 调用方法，动态生成路由
        util.initRouter(this);
    }
});
