import userApi from '@/api/user'
import util from '@/libs/util'
export default{
    state: {
        userName:'',
        userId:'',
        avatorImgPath:'',
        token:util.getToken(),
        access:''
    },
    mutations: {
    setAvator (state, avatorPath) {
      state.avatorImgPath = avatorPath
    },
    setUserId (state, id) {
      state.userId = id
    },
    setUserName (state, name) {
      state.userName = name
    },
    setAccess (state, access) {
      state.access = access
    },
    setToken (state, token) {
      state.token = token
      util.setToken(token)
    }
    },
    actions:{
        // 登录
        handleLogin({commit},{userName, password}){
            userName=userName.trim();
            commit('setToken','Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxIiwiYXVkIjoiQURNSU4iLCJleHAiOjE2MjA3OTE3NDB9.goscxyZIHST62zKVEiJUMlZFKivjn-SovhTDrT95qmtkhQeCt1lifZu7fclPXHKj02TqEHHhl50gp7rcyISO2A')
            // return new Promise((resolve,reject)=>{
            //     userApi.login({userName,password}).then(res=>{
            //         const data= res.data
            //         commit('setToken',data.token)
            //         resolve()
            //     }).catch(err=>{
            //         reject(err)
            //     })
            // })
        },
        // 退出登录
        handleLogOut(){

        }
    }
}
