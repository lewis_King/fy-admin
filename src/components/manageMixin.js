import Api from '@/api/index' //动态获取api
export default {
    data(){
        return{
            dataLoading:true,//数据加载
            isShow:true,//标题
            action:'',//是否增加、编辑状态
            name:'',//当前页
            searchValue:'',//查询值
            page:{
                current:1,
                total:0,
                size:20,
                op:[10, 20, 30, 40,50,100]
            },
            parentPath:'',//父级路径
            insideTableData:[] //table的数组
        }
    },
    methods:{
        //动态删除列表
        delList(name,id,index){
            Api[this.parentPath][name](id).then(res=>{
                 this.remove(index)
                 this.$Message.info(res.msg);
            })
        },
        //返回、取消
        goBack(isShow){
            this.isShow = !isShow
            this.getList(this.page.current)
            this.$cookie.remove('status');
        },
        //点击新增，编辑
        updateOp(isShow,action){
            let params={};
            this.isShow = !isShow;
            this.action=action;
            params.action=action;
            params.name=this.name
            this.$cookie.set('status',params);
        },
        //点击分页
        changePage(e){
            this.page.current=e
        },
        //点击搜索
        handleSearch () {
            this.insideTableData=this.insideTableData.filter(item=> item.username.indexOf(this.searchValue)>-1)
        },
        //清空搜索
        handleClear (e) {
            if (e.target.value === '') this.insideTableData=this.value
        },
        //不刷新删除列表
        remove (index) {
            this.insideTableData.splice(index, 1);
        },
        //初始化配置
        init(){
            this.name=this.$store.state.app.currentPageName
            this.value=this.insideTableData.slice(0);
            let cookie=this.$cookie.get('status');
            this.parentPath=this.$store.state.app.fullPath.split('/')[1] //获取父级路径
            if(cookie){
                let value=JSON.parse(cookie)
                if(value.name === this.name){
                    this.isShow=false
                    this.action=value.action
                }else{
                    this.isShow=true
                    this.action=''
                }
            }
        },
        //动态拉取列表请求
        getList(current){
            //例如 Api.business.consultManage(0).then(res=>{})
            Api[this.parentPath][this.name](current).then(res=>{
                const data = res.body
                this.insideTableData=data.records
                this.page.total=data.total
                this.page.size=data.size
                this.page.current=data.current
                this.dataLoading=false
            })
        }
    },
    created(){
        this.init()
        if(this.isShow)  this.getList(this.page.current)
    },
    watch:{
        'page.current' (to,from){
            this.getList(to)
        }
    }
}